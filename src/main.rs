use actix_web::{web, App, HttpResponse, HttpServer, Responder, get, post};
use serde::{Deserialize, Serialize};
use std::io::Error;
use std::fmt;
use serde_json::json;
use cdrs::frame::traits::TryFromRow;
use cdrs::types::from_cdrs::FromCDRSByName;
use cdrs::types::prelude::Row;

#[macro_use]
extern crate cdrs_helpers_derive;

mod db_handler;

#[derive(Deserialize, Serialize)]
struct Body {
    username: String,
    address: Option<String>,
}

#[derive(Clone, Debug, TryFromRow, PartialEq)]
struct RowStruct {
  id: String,
}

//impl fmt::Display for Body {
//    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//        // Write strictly the first element into the supplied output
//        // stream: `f`. Returns `fmt::Result` which indicates whether the
//        // operation succeeded or failed. Note that `write!` uses syntax which
//        // is very similar to `println!`.
//        write!(f, "lol {}", self.username)
//    }
//}

#[get("/hello")]
fn index3() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

#[post("/getHello")]
fn get_hello(name: web::Json<Body>) -> Result<web::Json<Body>, Error> {
//    Ok(format!("Welcome {}!", name.username))
//    Ok(format!("{}", web::Json(Body{
//        username: "bill".to_string(),
//        address: Some("bill as well".to_string()),
//    })))

    Ok(web::Json(name.0))

//    Ok(json!({
//        "name": name.username
//    }).to_string())
}

fn index() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

//fn index4(info: web::Json<Info>) -> Result<String, Error> {
//    Ok(format!("Welcome {}!", info.username))
//}

fn index2() -> impl Responder {
    HttpResponse::Ok().body("Hello world again!")
}

fn create_keyspace() -> impl Responder {
    match db_handler::create_keyspace() {
        Ok(_) => HttpResponse::Ok().body("Looks like it worked!"),
        Err(error) => HttpResponse::InternalServerError().body(format!("{}", error))
    }
}

fn create_table() -> impl Responder {
    match db_handler::create_table() {
        Ok(_) => HttpResponse::Ok().body("Looks like it worked!"),
        Err(error) => HttpResponse::InternalServerError().body(format!("{}", error))
    }
}

fn write_to_database() -> impl Responder {
    match db_handler::write() {
        Ok(_) => HttpResponse::Ok().body("Looks like it worked!"),
        Err(error) => HttpResponse::InternalServerError().body(format!("{}", error))
    }
}

fn read_from_database() -> impl Responder {
    let maybe_rows = match db_handler::read() {
        Ok(maybe_rows) => maybe_rows,
        Err(error) => return HttpResponse::InternalServerError().body(format!("{}", error))
    };

    let rows = match maybe_rows {
        Some(maybe_rows) => maybe_rows,
        None => return HttpResponse::Ok().body("No data found")
    };

    for row in rows {
        let my_row: RowStruct = RowStruct::try_from_row(row).expect("into RowStruct");
        println!("struct got: {:?}", my_row);
    }

    HttpResponse::Ok().body("Looks like it worked!")
}

fn main() {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .route("/again", web::get().to(index2))
            .route("/create/keyspace", web::get().to(create_keyspace))
            .route("/create/table", web::get().to(create_table))
            .route("/create/row", web::get().to(write_to_database))
            .route("/read", web::get().to(read_from_database))
            .service(index3)
//            .route("/", web::post().to(index4))
            .service(get_hello)
    })
        .bind("127.0.0.1:8088")
        .unwrap()
        .run()
        .unwrap();
}

