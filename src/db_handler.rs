use cdrs::authenticators::NoneAuthenticator;
use cdrs::cluster::session::{new as new_session, Session};
use cdrs::cluster::{ClusterTcpConfig, NodeTcpConfigBuilder, TcpConnectionPool};
use cdrs::load_balancing::RoundRobin;
use cdrs::query::*;
use std::time::Duration;

use cdrs::types::prelude::Row;

use std::error::Error;

type CurrentSession = Session<RoundRobin<TcpConnectionPool<NoneAuthenticator>>>;

// pub struct DatabaseHandler {
//     // db_session: CurrentSession,
//     var1: String,
// }

    // fn new() -> DatabaseHandler {
    //     // let node = NodeTcpConfigBuilder::new("127.0.0.1:9042", NoneAuthenticator {})
    //     //     .connection_timeout(Duration::from_secs(6))
    //     //     .build();

    //     // println!("setting cluster config");
    //     // let cluster_config = ClusterTcpConfig(vec![node]);

    //     // println!("creating session");
    //     // let no_compression =
    //     //     new_session(&cluster_config, RoundRobin::new()).expect("session should be created");

    //     DatabaseHandler{var1: String::new()}
    // }


pub fn create_keyspace() -> Result<(), Box<dyn Error>> {
    let database_connection = connect_to_database()?;
    let create_ks: &'static str = "CREATE KEYSPACE IF NOT EXISTS test_ks WITH REPLICATION = { \
                                'class' : 'SimpleStrategy', 'replication_factor' : 1 };";
    println!("Creating keyspace!");
    database_connection.query(create_ks)?;
    Ok(())
}

pub fn create_table() -> Result<(), Box<dyn Error>> {
    let database_connection = connect_to_database()?;
    let create_table: &'static str = "CREATE TABLE test_ks.cyclist_alt_stats ( id text PRIMARY KEY );";
    println!("Creating table!");
    database_connection.query(create_table)?;
    Ok(())
}

pub fn write() -> Result<(), Box<dyn Error>> {
    let database_connection = connect_to_database()?;
    let write_into_table: &'static str = "Insert into test_ks.cyclist_alt_stats(id) values ('bill')";
    println!("Writing into table!");
    database_connection.query(write_into_table)?;
    Ok(())
}

pub fn read() -> Result<Option<Vec<Row>>, Box<dyn Error>> {
    let database_connection = connect_to_database()?;
    let read_table: &'static str = "select * from  test_ks.cyclist_alt_stats";
    println!("reading table!");
    let rows = database_connection
        .query(read_table)?
        // .expect("read error")
        .get_body()?
        // .expect("body")
        .into_rows();
    Ok(rows)
        // .expect("rows");

    // for row in rows {
    //     let my_row: RowStruct = RowStruct::try_from_row(row).expect("into RowStruct");
    //     println!("struct got: {:?}", my_row);
    // }
    // Ok(())
}
// cdrs::load_balancing
fn connect_to_database() -> Result<CurrentSession, Box<dyn Error>> {
    let node = NodeTcpConfigBuilder::new("127.0.0.1:9042", NoneAuthenticator {})
        .connection_timeout(Duration::from_secs(1))
        .build();

    println!("setting cluster config");
    let cluster_config = ClusterTcpConfig(vec![node]);

    println!("creating session");
    let no_compression =
        // new_session(&cluster_config, RoundRobin::new()).expect("session should be created");
        new_session(&cluster_config, RoundRobin::new())?;

    Ok(no_compression)
}